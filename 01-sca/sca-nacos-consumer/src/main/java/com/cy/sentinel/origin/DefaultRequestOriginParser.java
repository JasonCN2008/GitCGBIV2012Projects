package com.cy.sentinel.origin;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class DefaultRequestOriginParser implements RequestOriginParser {
    //http://ip:port/path?origin=echo
    @Override
    public String parseOrigin(HttpServletRequest request) {
        //基于url中的参数进行限流
        // String origin=request.getParameter("origin");
        // return origin;
        //基于ip地址进行限流(假如是黑名单就限流了)
        //http://127.0.0.1:8090/consumer/doRestEcho
        String remoteAddr = request.getRemoteAddr();
        System.out.println("remoteAddr=" + remoteAddr);
        return remoteAddr;
    }//系统底层在运行时会对此方法的返回值与系统流控规则中的流控应用值进行比对
}
