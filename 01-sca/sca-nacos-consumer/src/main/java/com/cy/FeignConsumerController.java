package com.cy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@FeignClient("nacos-provider")
interface ConsumerHttpApi {
    @GetMapping("/provider/echo/{string}")
    public String echoMessage(@PathVariable(value = "string") String string);
}


@RestController
@RequestMapping("/consumer/feign/")
public class FeignConsumerController {

    @Value("${spring.application.name}")
    private String appName;

    @Autowired
    private ConsumerHttpApi consumerHttpApi;

    /**
     * 基于feign方式的服务调用
     */
    @GetMapping
    public String doFeignEcho() {
        return consumerHttpApi.echoMessage(appName);
    }
}
