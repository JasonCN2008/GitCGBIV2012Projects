package com.cy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class NacosConfigApplication {

    private static final Logger log =
            LoggerFactory.getLogger(NacosConfigApplication.class);

    @RefreshScope //配置信息动态刷新
    @RestController
    @RequestMapping("/config/")
    public class NacosConfigController {
        //@Value用于读取配置信息内容
        @Value("${logging.level.com.cy:info}")
        private String logLevel;

        @Value("${server.tomcat.threads.max:128}")
        private Integer maxThread;

        @Value("${page.pageSize:10}")
        private Integer pageSize;

        @GetMapping("doGetPageSize")
        public Integer doGetPageSize() {
            return pageSize;
        }

        @GetMapping("doGetMaxThread")
        public Integer doGetMaxThread() {
            return maxThread;
        }

        @GetMapping("doGetLogLevel")
        public String doGetLogLevel() {
            //System.out.println("doGetLogLevel()");
            log.debug("debug info");
            log.error("error info");
            return logLevel;
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(NacosConfigApplication.class, args);
    }
}
