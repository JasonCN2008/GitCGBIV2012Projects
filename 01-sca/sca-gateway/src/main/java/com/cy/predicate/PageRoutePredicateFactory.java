package com.cy.predicate;

import org.springframework.cloud.gateway.handler.predicate.AbstractRoutePredicateFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * 自定义谓词工厂,一定要基于官方规则进行实现
 */
@Component
public class PageRoutePredicateFactory
        extends AbstractRoutePredicateFactory<PageRoutePredicateFactory.PageConfig> {

    /**
     * 一定要在构造方法中调用父类带参构造函数，将我们的配置传到父类
     */
    public PageRoutePredicateFactory() {
        super(PageConfig.class);
    }

    /**
     * 获取谓词工厂中指定的参数顺序
     */
    @Override
    public List<String> shortcutFieldOrder() {
        //这里alist方法内部参数的顺序要与.yml配置中的配置的参数顺序一致。
        return Arrays.asList("minPage", "maxPage");
    }

    /**
     * 在此方法中定义谓词规则,校验逻辑，当方法的返回值为false时，停止对请求的转发
     */
    @Override
    public Predicate<ServerWebExchange> apply(PageConfig config) {
        return (serverWebExchange) -> {
            //获取请求url中传递的参数Page的值
            String pageStr =
                    serverWebExchange.getRequest().getQueryParams().getFirst("Page");
            //将Page的值转换为整数
            Integer page = Integer.parseInt(pageStr);
            //判定Page的范围
            return page > config.getMinPage() && page < config.getMaxPage();
        };
    }

    public static class PageConfig {
        private Integer minPage;
        private Integer maxPage;

        public Integer getMinPage() {
            return minPage;
        }

        public void setMinPage(Integer minPage) {
            this.minPage = minPage;
        }

        public Integer getMaxPage() {
            return maxPage;
        }

        public void setMaxPage(Integer maxPage) {
            this.maxPage = maxPage;
        }
    }
}
