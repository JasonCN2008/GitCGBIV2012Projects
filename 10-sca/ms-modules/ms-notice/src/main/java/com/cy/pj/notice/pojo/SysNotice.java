package com.cy.pj.notice.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * POJO(Plain Ordinary Java Object)对象
 * 通过此类对象封装公告相关数据,例如查询结果
 */
//@Setter
//@Getter
//@ToString
@Data  //setter,getter,toString,hashcode,equals
@NoArgsConstructor
@AllArgsConstructor
public class SysNotice {//SysNotice.class
    /**
     * 公告 ID
     */
    private Long id;
    /**
     * 公告标题
     */
    private String title;
    /**
     * 公告类型（1 通知 2 公告）
     */
    private String type;
    /**
     * 公告内容
     */
    private String content;
    /**
     * 公告状态（0 正常 1 关闭）
     */
    private String status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     *
     * @DateTimeFormat注解用于描述属性或set方法，用于告诉spring web模块，在将
     * 日期字符串转换为日期对象时，按照此注解中pattern属性设置的格式进行转换
     * @JsonFormat注解描述属性或get方法时，用于告诉底层API，在将对象转换为json字符串时， 按照此注解中pattern属性指定的日期格式进行转换, 其中timezone用于指定时区
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdTime;
    /**
     * 修改时间
     */
    private Date modifiedTime;
    /**
     * 创建用户
     */
    private String createdUser;
    /**
     * 修改用户
     */
    private String modifiedUser;

}
