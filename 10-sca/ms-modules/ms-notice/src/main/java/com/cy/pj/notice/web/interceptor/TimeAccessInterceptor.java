package com.cy.pj.notice.web.interceptor;

import com.cy.pj.common.exception.ServiceException;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Clock;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * SpringWeb模块定义的拦截器，其接口规范为HandlerInterceptor。
 * 当前项目中我们要通过此拦截器对相关资源进行访问时间上的限制
 */
public class TimeAccessInterceptor implements HandlerInterceptor {
    /**
     * 后端handler方法执行之前执行
     *
     * @param request  请求对象
     * @param response 响应对象
     * @param handler  目标处理器对象
     * @return 表示是否对请求进行放行，是否继续执行后续handler方法
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("==preHandle==");
        //基于JDK中的LocalTime 获取当前时间对象
      //  LocalTime localTime=LocalTime.now();
        LocalTime localTime =
                LocalTime.now(Clock.system(ZoneId.of("Asia/Shanghai")));
        int hour = localTime.getHour();
        System.out.println("hour=" + hour);
        //if (hour < 9 || hour > 18)
           // throw new ServiceException("请在指定时间访问");
        return true;//true表示放行，false表示请求到此结束
    }
}
