package com.cy.pj.common.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletUtil {

    /**
     * 获取HttpServletRequest请求对象
     *
     * @return
     */
    public static HttpServletRequest getRequest() {
        return getServletRequestAttributes().getRequest();
    }

    public static HttpServletResponse getResponse() {
        return getServletRequestAttributes().getResponse();
    }

    /**
     * 通过spring提供的RequestContextHolder对象获取请求属性对象
     *
     * @return
     */
    public static ServletRequestAttributes getServletRequestAttributes() {
        // String className=
        // RequestContextHolder.getRequestAttributes().getClass().getName();
        //System.out.println("className="+className);
        return (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    }

}
