package com.cy.pj.notice.web.advice;

import com.cy.pj.notice.web.pojo.JsonResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @RestControllerAdvice 注解描述的类，为spring web模块定义的全局异常处理类。
 * 当我们在@RestController/@Controller注解描述的类或父类中没有处理异常，则
 * 系统会查找@RestControllerAdvice注解描述的全局异常处理类，可以通过此类中的
 * 异常处理方法对异常进行处理。
 */
@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger log =
            LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * @param e 此参数用于接收要处理的异常对象，通常会与@ExceptionHandler注解
     *          中定义的异常类型相同，或者是@ExceptionHandler 注解中定义异常类型的父类
     *          类型。
     * @return 封装了异常状态和信息的对象
     * @ExceptionHandler 注解描述的方法为异常处理方法，此注解中定义的异常类型
     * 为此方法可以处理的异常类型(包含这个异常类型的子类类型)
     */
//    @ExceptionHandler(ServiceException.class)
//    public JsonResult doHandleServiceException(ServiceException e){
//        e.printStackTrace();
//        log.error("exception {}",e.getMessage());
//        return new JsonResult(e);
//    }
    @ExceptionHandler(RuntimeException.class)
    public JsonResult doHandleRuntimeException(RuntimeException e) {
        e.printStackTrace();
        log.error("exception {}", e.getMessage());
        return new JsonResult(e);
    }
    //.......

}
