package com.cy.pj.notice.web.config;

import com.cy.pj.notice.web.interceptor.TimeAccessInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 定义spring web模块配置类
 */
@Configuration
public class SpringWebConfig implements WebMvcConfigurer {
    /**
     * 注册拦截器
     *
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new TimeAccessInterceptor())
                //设置要拦截的路径
                .addPathPatterns("/notice/*");
    }
}
