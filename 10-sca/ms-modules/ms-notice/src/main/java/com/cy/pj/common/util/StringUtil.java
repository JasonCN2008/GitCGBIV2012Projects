package com.cy.pj.common.util;

public class StringUtil {
    /**
     * 判定字符串是否为空(null,"")
     *
     * @param content
     * @return
     */
    public static boolean isEmpty(String content) {
        return content == null || "".equals(content);
    }
    //....
}
