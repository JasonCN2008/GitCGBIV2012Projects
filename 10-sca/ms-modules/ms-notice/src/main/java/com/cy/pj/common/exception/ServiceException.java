package com.cy.pj.common.exception;

/**
 * 定义业务异常对象，自己定义的异常对象，都建议：
 * 1)直接或间接的继承RuntimeException
 * 2)添加构造方法(参考父类构造方法）
 */
public class ServiceException extends RuntimeException {
    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }

    public ServiceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
