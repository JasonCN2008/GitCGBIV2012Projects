var Notice = {
    template: `<div id="app" class="container">
      <div v-show="isListShow">
    <div>
      <form id="queryForm" style="display: inline">
               <input type="text" v-model="title" placeholder="请输入标题">
               <input type="button" class="btn btn-primary btn-sm" value="查询" @click="doQueryNotices">
      </form>
      <button class="btn btn-primary btn-sm" @click="doShowOrHide(false,true)">添加</button>
    </div>
   <table class="table">
    <thead>
    <tr>
        <th>序号</th>
        <th>标题</th>
        <th>公告类型</th>
        <th>状态</th>
        <th>创建时间</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <tr v-for="(v,i) of notices" :key="i">
    <td>{{i+1}}</td>
    <td>{{v.title}}</td>
    <td>{{v.type}}</td>
    <td>{{v.status}}</td>
    <td>{{v.createdTime}}</td>
    <td><button class="btn btn-danger btn-xs" @click="doDeleteById(v.id)">delete</button>
    <button class="btn btn-warning btn-xs" @click="doFindById(v.id)">update</button></td>
   </tr>
  </tbody>
  </table>
    <div class="pagination">
           <button class="pre" @click="doJumpToPage">上一页</button>
           <button class="percent">{{pageCurrent}}/{{pageCount}}</button>
           <button class="next"  @click="doJumpToPage">下一页</button>
           <form id="pageForm" style="display: inline">
               <input type="text" v-model="pageCurrent">
               <input type="button" value="跳转" class="jump"  @click="doJumpToPage">
           </form>
    </div>
  </div>
   <div v-show="isEditShow" style="display: none">
    <form>
        <input type="hidden" v-model="id">
            <div class="form-group">
                <label>类型</label>
                <select  v-model="type" class="form-control"  placeholder="类型">
                    <option value="1">通知</option>
                    <option value="2">公告</option>
                </select>
            </div>
            <div class="form-group">
                <label for="titleId">title</label>
                <input type="text" v-model="title" class="form-control" id="titleId" placeholder="标题">
            </div>

            <div class="form-group">
                <label for="contentId">content</label>
                <textarea type="text" v-model="content" rows="3" class="form-control" id="contentId" placeholder="内容">
                </textarea>
            </div>
            <div class="form-group">
                <label> 状态：</label>
                <label class="radio-inline"><input type="radio" v-model="status" name="status" value="0" checked> 打开</label>
                <label class="radio-inline"><input type="radio" v-model="status" name="status" value="1"> 关闭</label>
            </div>
            <div class="box-footer">
                <input type="button" @click="doSaveOrUpdate" value="Save" class="btn btn-primary"/>
                <input type="button" @click="doCancel" value="Cancel" class="btn btn-primary"/>
            </div>
      </form>
      </div>
</div>`,
    data() {
        return {
            notices: {},
            id: "",
            type: 1,
            title: "",
            content: "",
            status: 0,
            pageCurrent: 1,
            pageCount: 0,
            rowCount: 0,
            isListShow: true,
            isEditShow: false
        }
    },
    methods: {
        //基于分页按钮点击事件，进行分页查询
        doJumpToPage(event) {
            //1.获取被点击对象的class属性值.
            //$(this)表示被点击的对象
            //prop(属性名[,属性值])为jquery中用于操作属性值的一函数
            let cls = event.target.getAttribute("class");
            //2.基于被点击对象的class属性值修改当前页码值
            //2.1获取当前页码值，总页数
            //2.2修改当前页码值
            if (cls == "pre" && this.pageCurrent > 1) {
                this.pageCurrent--;
            } else if (cls == "next" && this.pageCurrent < this.pageCount) {
                this.pageCurrent++;
            } else if (cls == "jump") {
                if (this.pageCurrent < 1 || this.pageCurrent > this.pageCount) {
                    alert("页码值不合法");
                    this.pageCurrent = "";//回到初始状态
                    return;
                }
            } else {
                return;
            }
            //3.基于新的页码值重新执行查询
            this.doGetNotices();
        },
        doShowOrHide(list, edit) {
            this.isListShow = list;
            this.isEditShow = edit;
        },
        doQueryNotices() {
            this.pageCurrent = 1;
            this.doGetNotices();
        },
        doGetNotices() {
            let url = `http://localhost:9000/notice/?pageSize=3&pageCurrent=${this.pageCurrent}&title=${this.title}`;
            console.log("doGetObjects.url", url);
            axios.get(url).then((result) => {
                let respData = result.data.data;
                this.notices = respData.list;
                this.rowCount = respData.total;
                this.pageCount = respData.pages;
                this.pageCurrent = respData.pageNum;
            })
        },
        doFindById(id) {
            let url = `http://localhost/notice/${id}`;
            axios.get(url).then((result) => {
                this.id = result.data.data.id;
                this.title = result.data.data.title;
                this.content = result.data.data.content;
                this.type = result.data.data.type;
                this.status = result.data.data.status;
                this.doShowOrHide(false, true);
            })
        },
        doDeleteById(id) {
            let url = `http://localhost:9000/notice/${id}`;
            axios.delete(url).then((result) => {
                alert(result.data.message);
                this.doGetNotices();
            })
        },
        doSaveOrUpdate() {
            let url = "http://localhost/notice/";
            let params = {
                "id": this.id,
                "type": this.type,
                "title": this.title,
                "content": this.content,
                "status": this.status
            };
            if (this.id) {
                axios.put(url, params).then((result) => {
                    alert(result.data.message);
                    this.doGetNotices();
                    this.doReset();
                });
            } else {
                axios.post(url, params).then((result) => {
                    alert(result.data.message);
                    this.doGetNotices();
                    this.doReset();
                });
            }
        },
        doCancel() {
            this.doShowOrHide(true, false);
        },
        doReset() {
            this.id = "";
            this.type = 1;
            this.title = "";
            this.content = "";
            this.status = 0;
            this.isListShow = true;
            this.isEditShow = false;
        }
    },
    mounted: function () {
        this.doGetNotices();
    }
}