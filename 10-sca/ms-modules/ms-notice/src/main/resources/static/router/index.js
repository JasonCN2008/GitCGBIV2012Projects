var index = new VueRouter({
    routes: [
        {path: "/", component: index},
        {path: "/notice/", component: Notice},
        {path: "/detail/:id", component: detail, props: true},
        {path: "*", component: notFound}
    ]
});