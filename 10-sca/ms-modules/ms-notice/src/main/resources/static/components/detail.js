var detail = {
    props: ["id"],
    template: `<div class="detail">
        <h1 style="color:blue">这里是详情页</h1> 
        <h4 style="color:green">显示{{id}}号商品的详细信息...</h4>
        <button @click="back">返回首页</button>
    </div>`,
    methods: {
        back() {
            this.$router.push("/")
        }
    }
}