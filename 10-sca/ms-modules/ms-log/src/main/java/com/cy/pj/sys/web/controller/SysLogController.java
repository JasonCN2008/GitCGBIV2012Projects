package com.cy.pj.sys.web.controller;

import com.cy.pj.common.util.PageUtil;
import com.cy.pj.sys.pojo.SysLog;
import com.cy.pj.sys.service.SysLogService;
import com.cy.pj.sys.web.pojo.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/log/")
public class SysLogController {

    @Autowired
    private SysLogService sysLogService;

    @PostMapping
    public void doSaveLog(@RequestBody SysLog entity) {
        sysLogService.saveLog(entity);
    }

    /**
     * 基于id删除日志信息
     *
     * @param id
     * @return http://localhost/log/1,2,3
     */

    @DeleteMapping("{id}")
    public JsonResult doDeleteById(@PathVariable Long... id) {
        sysLogService.deleteById(id);
        return new JsonResult("delete ok");
    }

    /**
     * 基于id查询日志信息
     *
     * @param id
     * @return url: http://localhost/log/15
     */
    @GetMapping("{id}")
    public JsonResult doFindById(@PathVariable Long id) {
        return new JsonResult(sysLogService.findById(id));
    }

    /**
     * 基于条件分页查询日志信息
     *
     * @param sysLog
     * @return url: http://localhost/log/
     * http://localhost/log/?username=jason
     * http://localhost/log/?username=jason&status=1
     * http://localhost/log/?pageCurrent=1&pageSize=5
     */
    @GetMapping
    public JsonResult doFindLogs(SysLog sysLog) {
//        PageInfo<SysLog> pageInfo=
//                PageUtil.startPage()
//                        .doSelectPageInfo(
//                                ()->sysLogService.findLogs(sysLog));
//        return new JsonResult(pageInfo);
        return new JsonResult(
                PageUtil.startPage()//启动分页查询拦截
                        .doSelectPageInfo(() -> { //分页查询日志信息
                            //调用业务层方法执行查询，查询结果底层会存储到PageInfo对象
                            //String proxyCls = sysLogService.getClass().getName();
                            //System.out.println("proxyCls="+proxyCls);
                            sysLogService.findLogs(sysLog);
                            //假如应用了aop设计,sysLogService变量指向的是代理对象
                        }));
        //   return new JsonResult(sysLogService.findLogs(sysLog));
    }
}
