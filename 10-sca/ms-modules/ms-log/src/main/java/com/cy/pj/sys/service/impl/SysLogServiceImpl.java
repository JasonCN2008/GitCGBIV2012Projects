package com.cy.pj.sys.service.impl;

import com.cy.pj.common.annotation.RequiredTime;
import com.cy.pj.sys.dao.SysLogDao;
import com.cy.pj.sys.pojo.SysLog;
import com.cy.pj.sys.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 用户日志业务规范的实现
 */
@Transactional(readOnly = false,
        timeout = 60,
        rollbackFor = Throwable.class,
        isolation = Isolation.READ_COMMITTED)
@Service
public class SysLogServiceImpl implements SysLogService {

    @Autowired
    private SysLogDao sysLogDao;

//  public SysLogServiceImpl(SysLogDao sysLogDao){
//        this.sysLogDao=sysLogDao;
//  }

    @Transactional(readOnly = true)
    @RequiredTime
    @Override
    public List<SysLog> findLogs(SysLog sysLog) {
        System.out.println("log.find.thread->" + Thread.currentThread().getName());
        return sysLogDao.selectLogs(sysLog);
        //throw new RuntimeException("没有找到对应的记录");
    }

    @Override
    public SysLog findById(Long id) {
        return sysLogDao.selectById(id);
    }

    /**
     * @param sysLog
     * @Async注解描述的方法为一个异步切入点方法，这个方法执行时底层会在 新的线程中进行调用
     * @Async注解描述的方法其返回值类型建议为void,假如为非void你需要对方法 返回值类型进行设计，比方说方法声明为Future<Integer>,方法内部的返回值
     * 为 return new AsyncResult<Integer>(rows)
     */
    @Async
    @Override
    public void saveLog(SysLog sysLog) {
        System.out.println("save.log.thread->" + Thread.currentThread().getName());
        //模拟耗时操作
        //try{Thread.sleep(10000);}catch (Exception e){}
        //try{TimeUnit.SECONDS.sleep(10);}catch (Exception e){}
        sysLogDao.insertLog(sysLog);
    }

    @Transactional //此注解描述方法时，这个方法为一个事务切入点方法
    //@RequiredLog(operation = "删除日志")
    @Override
    public int deleteById(Long... id) {
//       for(int i=0;i<id.length;i++){
//            if(i==1){
//                //这里抛出异常时如何让已删除的数据回滚
//                throw new RuntimeException("删除失败");
//            }
//            sysLogDao.deleteById(id[i]);
//       }
        int rows = sysLogDao.deleteById(id);
        return rows;
    }
    //作业：springboot工程中 AOP方式的事务控制，去查@Transactional注解的应用
}
