package com.cy.pj.sys.service;

import com.cy.pj.sys.pojo.SysLog;

import java.util.List;

/**
 * 用户操作日志业务层接口，此接口中定义的是用户操作日志的业务规范
 */
public interface SysLogService {
    /**
     * 基于条件查询日志信息
     *
     * @param sysLog
     * @return
     */
    List<SysLog> findLogs(SysLog sysLog);

    /**
     * 基于id查询日志信息
     *
     * @param id
     * @return
     */
    SysLog findById(Long id);

    /**
     * 新增日志信息
     *
     * @param sysLog
     */
    void saveLog(SysLog sysLog);

    /**
     * 基于id删除日志记录
     *
     * @param id
     * @return
     */
    int deleteById(Long... id);

}
