package com.cy.pj.sys.web.advice;

import com.cy.pj.sys.web.pojo.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//@ResponseBody
//@ControllerAdvice
@Slf4j
@RestControllerAdvice //==@ControllerAdvice+@ResponseBody
public class GlobalExceptionHandler {

    @ExceptionHandler(NumberFormatException.class)
    public JsonResult doHandleNumberFormatException(NumberFormatException e) {
        e.printStackTrace();
        log.error("exception {}", "你传入参数值类型不匹配");
//        return new JsonResult(e);
//        JsonResult r=new JsonResult();
//        r.setState(0);
//        r.setMessage("你传入参数值类型不匹配");
//        return r;
        return new JsonResult(0, "你传入参数值类型不匹配");
    }

    @ExceptionHandler(RuntimeException.class)
    public JsonResult doHandleRuntimeException(RuntimeException e) {
        e.printStackTrace();
        log.error("exception {}", e.getMessage());
        return new JsonResult(e);
    }
}
