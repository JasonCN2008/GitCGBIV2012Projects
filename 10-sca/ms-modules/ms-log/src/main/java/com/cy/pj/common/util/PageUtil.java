package com.cy.pj.common.util;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import javax.servlet.http.HttpServletRequest;

public class PageUtil {

    public static <T> Page<T> startPage() {
        HttpServletRequest request = ServletUtil.getRequest();
        String pageCurrentStr = request.getParameter("pageCurrent");//abc
        String pageSizeStr = request.getParameter("pageSize");
        int pageCurrent = StringUtil.isEmpty(pageCurrentStr) ? 1 : Integer.parseInt(pageCurrentStr);
        int pageSize = StringUtil.isEmpty(pageSizeStr) ? 5 : Integer.parseInt(pageSizeStr);
        return PageHelper.startPage(pageCurrent, pageSize);
    }
}
