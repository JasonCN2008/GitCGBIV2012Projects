package com.cy.pj.log.service;

import com.cy.pj.log.pojo.SysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@FeignClient(value = "log-app")
public interface RemoteLogService {

    @PostMapping("/log/")
    void saveLog(@RequestBody SysLog sysLog);
}
