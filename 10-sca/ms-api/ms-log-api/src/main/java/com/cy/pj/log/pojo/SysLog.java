package com.cy.pj.log.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class SysLog implements Serializable {
    private static final long serialVersionUID = -2100665864616613205L;
    private Long id;
    private String ip;
    private String username;
    private String operation;
    private String method;
    private String params;
    private Long time;
    private Integer status;
    private String error;
    private Date createdTime;
}
