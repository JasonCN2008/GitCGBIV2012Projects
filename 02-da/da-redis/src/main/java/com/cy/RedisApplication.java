package com.cy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.StringRedisTemplate;

@SpringBootApplication
public class RedisApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(RedisApplication.class, args);
    }

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //springboot项目启动时会调用CommandLineRunner对象的run方法，
    @Override
    public void run(String... args) throws Exception {
        System.out.println("stringRedisTemplate=" + stringRedisTemplate);
        String name = stringRedisTemplate.opsForValue().get("name");
        // System.out.println("name="+name);
    }
}
