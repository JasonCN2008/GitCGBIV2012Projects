package com.cy;


import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@SpringBootTest
public class StringRedisTemplateTests {
    /**
     * 此对象为spring提供的一个用于操作redis数据库中的字符串的一个对象
     */
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Test
    void testOpsForValueSet() {
        stringRedisTemplate.opsForValue().set("age", "18");
        stringRedisTemplate.opsForValue().set("id", "111", 5, TimeUnit.SECONDS);
        System.out.println("set ok");
        Map<String, String> map = new HashMap<>();
        map.put("id", "100");
        map.put("title", "hello redis");
        map.put("content", "redis is very good");
        String jsonStr = JSON.toJSONString(map);
        //存json格式字符串
        stringRedisTemplate.opsForValue().set("message", jsonStr);
    }

    @Test
    void testOpsForValueGet() {
        String name = stringRedisTemplate.opsForValue().get("name");
        System.out.println("name=" + name);
        String age = stringRedisTemplate.opsForValue().get("age");
        System.out.println("age=" + age);
        String id = stringRedisTemplate.opsForValue().get("id");
        System.out.println("id=" + id);
        String message = stringRedisTemplate.opsForValue().get("message");
        Map<String, String> map = (Map<String, String>) JSON.parse(message);
        System.out.println(map);
    }
}
