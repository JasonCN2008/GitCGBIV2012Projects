package com.cy;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.test.context.SpringBootTest;
import redis.clients.jedis.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class JedisTests {
    @Test
    void testRedisShard() {//客户端分片机制的实现，在客户端拿到所有服务实例
        //Jedis连接池的配置
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxTotal(200);
        //Jedis中的具体分片对象
        JedisShardInfo info1 = new JedisShardInfo("192.168.64.129", 6379);
        JedisShardInfo info2 = new JedisShardInfo("192.168.64.129", 6380);
        List<JedisShardInfo> jedisShardInfos = new ArrayList<>();
        jedisShardInfos.add(info1);
        jedisShardInfos.add(info2);

        //redis分片关键对象ShardedJedisPool
        ShardedJedisPool shardedJedisPool =
                new ShardedJedisPool(jedisPoolConfig, jedisShardInfos);
        for (int i = 0; i < 10; i++) {
            ShardedJedis resource = shardedJedisPool.getResource();
            //将name+i做为key,i对应的字符串作为value写入到redis中
            resource.set("name" + i, String.valueOf(i));
            //分片存储时有一些算法，可以让数据动态存储到不同的redis实例中
            //按指定范围(0~5,6~10),按一致性hash算法进行分片存储
        }
        shardedJedisPool.close();
    }

    @Test
    void testGet6379() throws Exception {
        Jedis jedis = new Jedis("192.168.64.129", 6379);
        String name = jedis.get("name");
        System.out.println("jedis.name=" + name);
    }

    @Test
    void testGet6380() throws Exception {
        Jedis jedis = new Jedis("192.168.64.129", 6380);
        String name = jedis.get("name");
        System.out.println("jedis.name=" + name);
    }
}
